import React, {useState, useEffect} from 'react'
import {Text, View, Dimensions, StyleSheet, Image, ScrollView , TouchableOpacity } from 'react-native';
import Colors from '../../style/Colors';

const {width} = Dimensions.get('window');

const AddButton = (props) => {

    const { title, onPress } = props; 

    return (
        <TouchableOpacity activeOpacity={0.5} onPress={onPress} style={styles.button} >
            <Text style={styles.buttonText} >{title}</Text>
        </TouchableOpacity> 
    )
}

const styles = StyleSheet.create({
    button: {
        height: 50,
        marginLeft: 15,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 8,
        padding: 6,
        backgroundColor: Colors.ORANGE, 
    },
    buttonText: {
        fontSize: 18,
        lineHeight: 22,
        fontWeight: '700',
        color: Colors.WHITE
    }
})


export default AddButton