import React, {useState, useEffect} from 'react'
import {Text, View, Dimensions, StyleSheet, Image, TextInput, ScrollView , TouchableOpacity } from 'react-native';
import Colors from '../../style/Colors';

const {width} = Dimensions.get('window');

const Input = (props) => {

    const { placeholder, value, onChange, error,} = props; 

    return (
        <View style={{width: '100%'}} >
            <TextInput 
                placeholder={placeholder}
                onChangeText={onChange}
                value={value}
                style={styles.input}
            />

            <Text style={styles.error} >{error ? error : null}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
       
    },
    input: {
        height: 50, 
        borderWidth: 1, 
        borderRadius: 4,
        paddingLeft: 15,
        fontSize: 16,
        fontWeight: '500',
        borderColor: Colors.GRAY,
    },
    error: {
        fontSize: 14,
        lineHeight: 18,
        marginTop: 2,
        color: Colors.RED
    },
})


export default Input