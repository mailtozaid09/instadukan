import React, {useState, useEffect} from 'react'
import {Text, View, Dimensions, StyleSheet, FlatList, Image, TextInput, ScrollView , TouchableOpacity } from 'react-native';
import Colors from '../style/Colors';

import { observer } from 'mobx-react';
import Input from '../components/input/Input';
import AddButton from '../components/button/AddButton';
import { todoStore } from '../store/todostore';

const {width} = Dimensions.get('window');

const HomeScreen = observer(({navigation}) => {

	const [name, setName] = useState('');

    return (
        <View style={styles.mainContainer} >
            <View style={styles.container} >
                <View style={styles.inputContainer} >
                    <View style={styles.input} >
                        <Input
                            value={name}
                            onChange={(text) => setName(text)}
                            placeholder="Enter the to-do name"
                        />
                    </View>
                    <AddButton title="Add" onPress={() => {todoStore.addTodo({name}); setName('')}} />
                </View>

                <FlatList
                    data={todoStore.todolist}
                    keyExtractor={(item) => item.id}
                    style={{ width: '100%', paddingBottom: 10 }}
                    renderItem={({ item }) => (
                        <View style={styles.cardContainer} >
                            <View style={styles.todoCard} >
                                <View style={styles.textContainer} >
                                    <Text style={styles.textStyle} >{`${item.name}`}</Text>
                                </View>
                                <TouchableOpacity onPress={() => todoStore.deleteTodo(item.id)}>
                                    <Image source={require('../../assets/delete.png')} style={styles.delete} />
                                </TouchableOpacity>
                            </View>
                        </View>
                    )}
                />
            </View>
        </View>
    )
})

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
    },
    container: {
        flex: 1,
        padding: 20
    },
    inputContainer: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'space-between'
    },
    input: { 
        flex :1,
    },
    todoCard: {
        padding: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    delete: {
        height: 24,
        width: 24,
    },
    cardContainer: {
        marginTop: 12, 
        borderWidth: 1, 
        padding: 8, 
        borderRadius: 10, 
        backgroundColor: Colors.WHITE, 
        justifyContent: 'center', 
    },
    textContainer: {
        width: '100%', 
        flex: 1, 
        marginRight: 15,
    },
    textStyle: {
        fontSize: 20,
        color: Colors.BLACK
    }
})

export default HomeScreen