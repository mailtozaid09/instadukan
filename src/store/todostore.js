import { action, computed, makeObservable, observable } from "mobx"

class TodoList{
    todolist = []

    constructor(){
        makeObservable(this,{
            todolist: observable,
            addTodo: action,
            deleteTodo: action,
            count: computed
        })
    }

    addTodo(hero){
        this.todolist = [...this.todolist, {...hero, id: Math.random() }]
	}

    deleteTodo(id){
        this.todolist = [...this.todolist.filter(hero => hero.id !== id)]
	}

    get count(){
        return this.todolist.length
	}
}


export const todoStore = new TodoList()