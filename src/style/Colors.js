const Colors = {
    RED: '#d04848',
    BLACK: '#000000',
    WHITE: '#FFFFFF',
    GRAY: '#A9A9A9',
    YELLOW: '#f2b443',
    PURPLE: '#6D5EF6',
    CREAM: '#fcd5aa',
    ORANGE: '#f26122',
    BLUE: '#0474ff',
    DARK_GRAY: '#666666',
    LIGHT_BLUE: '#b5ddea',
    LIGHT_GRAY: '#ededed',
    LIGHT_ORANGE: '#fdd9cb',
};

export default Colors;